import re
from urllib.request import urlopen

def find_str(num):

    # check if str is in html page
    # two option of tests:
    if num == 1:
        text = "Right-click in the box below to see one called 'the-internet'"
    if num == 2:
        text = "Alibaba"    # test FAILED
        #text = "Menu"        # test PASSED
    url = "https://the-internet.herokuapp.com/context_menu"
    page = urlopen(url)
    html_bytes = page.read()
    html = html_bytes.decode("utf-8")
    result = re.findall(text, html)
    if result:
        return True
    else:
        return False

if __name__ == "__main__":
    find_str()
