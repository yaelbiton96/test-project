from src.main import find_str

# PASSED: test look for "Right-click in the box below to see one called 'the-internet'"
def test_one():
    assert find_str(1) == True

# FAILED: test look for "Alibaba"
def test_two():
    assert find_str(2) == True
